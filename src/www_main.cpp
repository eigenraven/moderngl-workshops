#define WWW_UTILS_IMPL
#define STB_IMAGE_IMPLEMENTATION
#define TINYOBJLOADER_IMPLEMENTATION
#include "wwwinc.h"
#include "www_utils.h"

AState* state;

AState::~AState()
{
}

typedef AState* (*stateFactoryFp)();
stateFactoryFp STATES[] = {
	nullptr,
	&makeState1,
	&makeState2,
    &makeState3,
    &makeState4,
    &makeState5,
    &makeState6
};

int main(int argc, char** argv)
{
    stbi_set_flip_vertically_on_load(1);
    createWindow();
    glfwSetTime(0.0);
    double dt = 0.1;
	//
	int lim = sizeof(STATES) / sizeof(STATES[0]);
	if(argc<2)
    	state = STATES[lim-1]();
	else
	{
		int prog = atoi(argv[1]);
		if((prog <= 0) || (prog >= lim))
		{
			fprintf(stderr, "Uzycie: %s {przyklad}\n", argv[0]);
			return 1;
		}
		state = STATES[prog]();
	}
	//
    int frames = 0;
    double framet = 0.0;
    while(!glfwWindowShouldClose(window))
    {
        framet += dt;
        frames++;
        if(framet >= 5.0)
        {
            fprintf(stderr, "FPS: %.02f\n", float(frames/framet));
            frames = 0;
            framet = 0.0;
        }
        state->update(dt);
        state->draw(dt);
        GLenum glerr = glGetError();
        if(glerr != GL_NO_ERROR)
        {
            fprintf(stderr, "GL Error: %d\n", glerr);
        }
        glfwPollEvents();
        glfwSwapBuffers(window);
        dt = glfwGetTime();
        glfwSetTime(0.0);
    }
	delete state;
	state = nullptr;
    glfwTerminate();
    return 0;
}
