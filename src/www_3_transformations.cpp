#include "wwwinc.h"
#include "www_utils.h"

class State3_TRF : public AState
{
	const GLchar* vertShader = R"GLSL(
		#version 330
		
		in vec2 position;

		uniform mat4 model;
		
		void main()
		{
			gl_Position = model * vec4(position, 0.0, 1.0);
		}
	)GLSL";

	const GLchar* fragShader = R"GLSL(
		#version 330
		
		out vec4 outColor;
		
		uniform float hue;
		
		vec3 hsv2rgb(vec3 c)
		{
			vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
			return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
		}
		
		void main()
		{
			outColor = vec4(hsv2rgb(vec3(hue, 1.0, 1.0)), 1.0);
		}
	)GLSL";
public:
	GLuint program;
	GLuint vsh, fsh;
	GLuint vbo, vao;
	GLuint uniformHue, uniformModelMat;
	double taccum = 0.0;

    State3_TRF()
    {
        glClearColor(0.0, 0.0, 0.0, 1.0);
		program = glCreateProgram();
		vsh = compileShader(GL_VERTEX_SHADER, vertShader, __FILE__ ":VSH");
		fsh = compileShader(GL_FRAGMENT_SHADER, fragShader, __FILE__ ":FSH");
		glAttachShader(program, vsh);
		glAttachShader(program, fsh);
		linkProgram(program, __FILE__ ": " STR(__LINE__));
		glUseProgram(program);
		fprintf(stderr, "Shader OK\n");
		
		float verts[] = {
			0.0f, 0.5f,
			0.5f, -0.5f,
			-0.5f, -0.5f
		};
		
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
		fprintf(stderr, "VBO OK\n");
		
		GLint posAttrib = glGetAttribLocation(program, "position");
		glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(posAttrib);
		
		uniformHue = glGetUniformLocation(program, "hue");
		uniformModelMat = glGetUniformLocation(program, "model");
    }

    virtual ~State3_TRF()
    {
    	fprintf(stderr, "Cleaning Up\n");
    	glDeleteProgram(program);
    	glDeleteShader(vsh);
    	glDeleteShader(fsh);
    	glDeleteBuffers(1, &vbo);
    	glDeleteVertexArrays(1, &vao);
    	fprintf(stderr, "CleanUp Finished\n");
    }

    void draw(double dt)
    {
    	static const float turn_time = 2.5f;
    	static const float circle_rad = 0.25f;
		taccum += dt;
		while(taccum >= max(1.f, turn_time))
			taccum -= max(1.f, turn_time);
		glUniform1f(uniformHue, float(fmod(taccum,1.f)));

		glm::mat4x4 modelMat;
		float rot_rad = glm::radians(360.f*float(taccum/turn_time));

		//this is just an example set of transformations
		//during workshops this can be altered acording to preferences of participants
		//i suggest adding this code in the order in the comments
		//-----------------------------------------------------------------------------------------
		//4<-showing that we can change the size of the triangle ignoring rotation and translation
		modelMat = glm::scale(modelMat, glm::vec3(0.5f, 0.5f, 1.f));											
		//3<-showing that we can translate igroning the rotation
		modelMat = glm::translate(modelMat, glm::vec3(circle_rad*cos(rot_rad),circle_rad*sin(rot_rad),0.f));
		//1<-rotation with respect to the center of the triangle
		modelMat = glm::rotate(modelMat, rot_rad, glm::vec3(0.f, 0.f, 1.f));
		//2<-showing that we can change the center of rotation
		modelMat = glm::translate(modelMat, glm::vec3(0.5f,0.5f,0.f));	
		
		glUniformMatrix4fv(uniformModelMat, 1, GL_FALSE, glm::value_ptr(modelMat));

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    void update(double dt)
    {

    }

    void onButton(int button, int state)
    {

    }

    void onKey(int id, int state)
    {

    }

};

AState* makeState3()
{
    return new State3_TRF();
}
