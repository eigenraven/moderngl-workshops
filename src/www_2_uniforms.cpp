#include "wwwinc.h"
#include "www_utils.h"

class State2_UNI : public AState
{
	const GLchar* vertShader = R"GLSL(
		#version 330

		uniform mat4 model;
		in vec2 position;

		void main()
		{
			gl_Position = model * vec4(position, 0.0, 1.0);
		}
	)GLSL";

	const GLchar* fragShader = R"GLSL(
		#version 330

		out vec4 outColor;

		uniform float hue;

		vec3 hsv2rgb(vec3 c)
		{
			vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
			vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
			return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
		}

		void main()
		{
			outColor = vec4(hsv2rgb(vec3(hue, 1.0, 1.0)), 1.0);
		}
	)GLSL";
public:
	GLuint program;
	GLuint vsh, fsh;
	GLuint vbo, vao;
	GLuint uniformHue;
	GLint uModel;
	glm::mat4 model;
	double taccum = 0.0;

	State2_UNI()
	{
		glClearColor(0.0, 0.0, 0.0, 1.0);
		program = glCreateProgram();
		vsh = compileShader(GL_VERTEX_SHADER, vertShader, __FILE__ ":VSH");
		fsh = compileShader(GL_FRAGMENT_SHADER, fragShader, __FILE__ ":FSH");
		glAttachShader(program, vsh);
		glAttachShader(program, fsh);
		linkProgram(program, __FILE__ ": " STR(__LINE__));
		glUseProgram(program);
		fprintf(stderr, "Shader OK\n");

		float verts[] = {
			0.0f, 0.5f,
			0.5f, -0.5f,
			-0.5f, -0.5f
		};

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
		fprintf(stderr, "VBO OK\n");

		GLint posAttrib = glGetAttribLocation(program, "position");
		glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(posAttrib);

		uModel = glGetUniformLocation(program, "model");

		uniformHue = glGetUniformLocation(program, "hue");
	}

	virtual ~State2_UNI()
	{
		fprintf(stderr, "Cleaning Up\n");
		glDeleteProgram(program);
		glDeleteShader(vsh);
		glDeleteShader(fsh);
		glDeleteBuffers(1, &vbo);
		glDeleteVertexArrays(1, &vao);
		fprintf(stderr, "CleanUp Finished\n");
	}

	void draw(double dt)
	{
		taccum += dt;
		while(taccum >= 360.0)
			taccum -= 360.0;
		model = glm::rotate(glm::mat4(), glm::radians((float)taccum*360.f), glm::vec3(0, 1, 0));
		glUniformMatrix4fv(uModel, 1, GL_FALSE, glm::value_ptr(model));
		glUniform1f(uniformHue, 20.f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glDrawArrays(GL_TRIANGLES, 0, 3);
	}

	void update(double dt)
	{

	}

	void onButton(int button, int state)
	{

	}

	void onKey(int id, int state)
	{

	}

};

AState* makeState2()
{
	return new State2_UNI();
}
