#pragma once

#include "wwwinc.h"

#define STR(X) #X

namespace std {
	template<> struct hash<pair<int,int> > {
		size_t operator()(pair<int,int> const& p) const {
			return hash<int>()(p.first) ^ (hash<int>()(p.second) << 1);
		}
	};
}

/**
 * Shader:
 * Vert:
	in vec3 position;
	in vec3 normal;
	in vec3 diffuse;
	in vec3 emission;
	in vec3 specular;
	in vec2 texcoords;
	in float alpha;

	out vec3 fPosition;
	out vec3 fNormal;
	out vec3 fDiffuse;
	out vec3 fEmission;
	out vec3 fSpecular;
	out vec2 fTexcoords;
	out float fAlpha;

	fNormal = normal;
	fDiffuse = diffuse;
	fEmission = emission;
	fSpecular = specular;
	fTexcoords = texcoords;
	fPosition = position;
	fAlpha = alpha;

	Frag:

	uniform int hasTexture;
	uniform sampler2D materialTexture;

	in vec3 fPosition;
	in vec3 fNormal;
	in vec3 fDiffuse;
	in vec3 fEmission;
	in vec3 fSpecular;
	in vec2 fTexcoords;
	in float fAlpha;
	out vec4 outColor;

 */

struct ObjObject
{
	/// Index into EBO
	int start;
	int numIndices;
	GLuint texture;
	bool hasTexture;
};

struct ObjVertex
{
	/// Position
	float x, y, z;
	/// Normal
	float nx, ny, nz;
	/// Diffuse Color
	float dr, dg, db;
	/// Emission Color
	float er, eg, eb;
	/// Specular Color
	float sr, sg, sb;
	/// Texture coords
	float u, v;
	/// Transparency
	float alpha;
};

struct ObjModel
{
	// OpenGL data
	GLuint vbo, ebo, vao;
	vector<int> textures;
	// Other data
	vector<ObjObject> objects;
	vector<tinyobj::shape_t> shapes;
	vector<tinyobj::material_t> materials;
};

/// Tworzy shader o danym typie, kompiluje kod i wychodzi z programu, jezeli znajdzie blad.
GLuint compileShader(GLenum shaderType, const GLchar* shaderSource, const char* location);

/// Laczy podany program i sprawdza, czy wystapily bledy
void linkProgram(GLuint id, const char* location);

GLint setAttrib(int program, string name, int dims, int type, int norm, int offset, int stride);

/// Cubemap : folder/{up,down,north,east,west,south}.png
GLuint loadCubemap(const char* folder);

GLuint loadTexture(string fpath);

ObjModel loadModel(string path, GLuint program);

// implementacja
#if defined(WWW_UTILS_IMPL) || 0

GLuint compileShader(GLenum shaderType, const GLchar* shaderSource, const char* location)
{
	GLuint id;
	id = glCreateShader(shaderType);
	glShaderSource(id, 1, &shaderSource, nullptr);
	glCompileShader(id);
	GLint status = 0;
	glGetShaderiv(id, GL_COMPILE_STATUS, &status);
	if(status != GL_TRUE)
	{
		char buffer[512];
		glGetShaderInfoLog(id, 512, nullptr, buffer);
		fprintf(stderr, "Shader compilation error (%s): %s\n", location, buffer);
		throw new runtime_error("Shader compilation error");
	}
	return id;
}

void linkProgram(GLuint id, const char* location)
{
	glLinkProgram(id);
	GLint status = 0;
	glGetProgramiv(id, GL_LINK_STATUS, &status);
	if(status != GL_TRUE)
	{
		char buffer[512];
		glGetProgramInfoLog(id, 512, nullptr, buffer);
		fprintf(stderr, "Shader linking error (%s): %s\n", location, buffer);
		throw new runtime_error("Shader compilation error");
	}
}

GLint setAttrib(int program, string name, int dims, int type, int norm, int offset, int stride)
{
	GLint attr = glGetAttribLocation(program, name.c_str());
	if(attr >= 0)
	{
		uint8_t* poff = nullptr;
		glVertexAttribPointer(attr, dims, type, norm, stride, poff + offset);
		glEnableVertexAttribArray(attr);
	}
	return attr;
}

GLuint loadCubemap(const char* folder)
{
	static map<string, GLenum> cubeFaces;
	if(cubeFaces.size()==0)
	{
		cubeFaces["down.png"] = GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
		cubeFaces["north.png"] = GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
		cubeFaces["east.png"] = GL_TEXTURE_CUBE_MAP_POSITIVE_X;
		cubeFaces["up.png"] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
		cubeFaces["south.png"] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;
		cubeFaces["west.png"] = GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
	}
	GLuint texid;
	glGenTextures(1, &texid);
	glBindTexture(GL_TEXTURE_CUBE_MAP, texid);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	for(auto kv : cubeFaces)
	{
		string ff = kv.first;
		GLenum trg = kv.second;
		string fpath = folder;
		if(fpath.back() != '/')
		{
			fpath += "/";
		}
		fpath += ff;

		int w,h,chan;
		unsigned char* data = stbi_load(fpath.c_str(), &w, &h, &chan, 3);

		glTexImage2D(trg, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		free(data);
	}
	return texid;
}

GLuint loadTexture(string fpath)
{
	stbi_set_flip_vertically_on_load(1);
	GLuint texid;
	glGenTextures(1, &texid);
	glBindTexture(GL_TEXTURE_2D, texid);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	int w,h,chan;
	unsigned char* data = stbi_load(fpath.c_str(), &w, &h, &chan, 3);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	free(data);
	return texid;
}

ObjModel loadModel(string path, GLuint program)
{
	ObjModel model = {};

	string err;
	if(!tinyobj::LoadObj(model.shapes, model.materials, err, path.c_str(), "res/"))
	{
		fprintf(stderr, "Error loading OBJ: %s\n", err.c_str());
		exit(1);
	}
	auto& scShapes = model.shapes;
	auto& scMaterials = model.materials;

	vector<ObjVertex> verts;
	vector<int> elements;
	typedef pair<int, int> vertKey; // (vertId, materialId)
	unordered_map<vertKey, int> vertIds;
	int totalVerts = 0;
	for(size_t ishape=0;ishape<scShapes.size();ishape++)
	{
		vertIds.clear();
		tinyobj::shape_t& shape = scShapes[ishape];
		tinyobj::mesh_t& mesh = shape.mesh;
		size_t numVerts = mesh.positions.size()/3;
		ObjObject dobj;
		dobj.numIndices = (int)mesh.indices.size();
		dobj.start = (int)elements.size();
		if(scMaterials[mesh.material_ids[0]].diffuse_texname.length() > 0)
		{
			dobj.texture = mesh.material_ids[0];
		}
		else
		{
			dobj.texture = -1;
		}
		for(size_t ii=0; ii<dobj.numIndices; ii++)
		{
			int face = ii/3;
			int matid = mesh.material_ids[face];
			int i = mesh.indices[ii];
			vertKey key = make_pair(i, matid);
			if(vertIds.count(key) == 0)
			{
				vertIds[key] = totalVerts;
				tinyobj::material_t& mat = scMaterials[matid];
				ObjVertex v;
				v.x = mesh.positions[3*i];
				v.y = -mesh.positions[3*i + 1];
				v.z = mesh.positions[3*i + 2];
				if(mesh.normals.size()>0)
				{
					v.nx = mesh.normals[3*i];
					v.ny = -mesh.normals[3*i + 1];
					v.nz = mesh.normals[3*i + 2];
				}
				if(mesh.texcoords.size()>0)
				{
					v.u = mesh.texcoords[2*i];
					v.v = mesh.texcoords[2*i + 1];
				}
				if(matid < scMaterials.size())
				{
					v.alpha = mat.dissolve;
					v.dr = mat.diffuse[0];
					v.dg = mat.diffuse[1];
					v.db = mat.diffuse[2];
					v.er = mat.emission[0];
					v.eg = mat.emission[1];
					v.eb = mat.emission[2];
					v.sr = mat.specular[0];
					v.sg = mat.specular[1];
					v.sb = mat.specular[2];
				}
				else
				{
					v.alpha = 1.0f;
					v.dr = 1.0f;
					v.dg = 0.0f;
					v.db = 1.0f;
					v.er = 0.5f;
					v.eg = 0.0f;
					v.eb = 0.5f;
					v.sr = 0.0f;
					v.sg = 0.5f;
					v.sb = 0.0f;
				}
				verts.push_back(v);
				elements.push_back(totalVerts);
				totalVerts ++;
			}
			else
			{
				elements.push_back(vertIds[key]);
			}
		}
		model.objects.push_back(dobj);
	}
	auto& materialTextures = model.textures;
	materialTextures.resize(scMaterials.size(), 0);
	for(int i=0; i<scMaterials.size();i++)
	{
		tinyobj::material_t& mat = scMaterials[i];
		if(mat.diffuse_texname.length()>0)
		{
			materialTextures[i] = loadTexture(string("res")+mat.diffuse_texname);
		}
	}
	printf("Loaded %d vertices, %d indices, %d materials.\n", int(verts.size()), int(elements.size()), int(scMaterials.size()));

	glGenVertexArrays(1, &model.vao);
	glBindVertexArray(model.vao);
	glGenBuffers(1, &model.vbo);
	glGenBuffers(1, &model.ebo);
	glBindBuffer(GL_ARRAY_BUFFER, model.vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.ebo);
	glBufferData(GL_ARRAY_BUFFER, verts.size()*sizeof(ObjVertex), verts.data(), GL_STATIC_DRAW);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size()*sizeof(int), elements.data(), GL_STATIC_DRAW);
	setAttrib(program, "position", 3, GL_FLOAT, GL_FALSE, offsetof(ObjVertex, x), sizeof(ObjVertex));
	setAttrib(program, "normal", 3, GL_FLOAT, GL_FALSE, offsetof(ObjVertex, nx), sizeof(ObjVertex));
	setAttrib(program, "diffuse", 3, GL_FLOAT, GL_FALSE, offsetof(ObjVertex, dr), sizeof(ObjVertex));
	setAttrib(program, "specular", 3, GL_FLOAT, GL_FALSE, offsetof(ObjVertex, sr), sizeof(ObjVertex));
	setAttrib(program, "emission", 3, GL_FLOAT, GL_FALSE, offsetof(ObjVertex, er), sizeof(ObjVertex));
	setAttrib(program, "texcoords", 2, GL_FLOAT, GL_FALSE, offsetof(ObjVertex, u), sizeof(ObjVertex));
	setAttrib(program, "alpha", 1, GL_FLOAT, GL_FALSE, offsetof(ObjVertex, alpha), sizeof(ObjVertex));

	return model;
}

#endif
