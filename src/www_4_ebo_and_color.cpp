#include "wwwinc.h"
#include "www_utils.h"

struct Vertex
{
	float x, y, z;
	float r, g, b;
};

class State4_EBOC : public AState
{
	const GLchar* vertShader = R"GLSL(
		#version 330

		in vec3 position;
		in vec3 vertColor;
		uniform mat4 model;
		uniform mat4 perspective;

		out vec3 fragColor;

		void main()
		{
			fragColor = vertColor;
			gl_Position = perspective * model * vec4(position, 1.0);
		}
	)GLSL";

	const GLchar* fragShader = R"GLSL(
		#version 330

		in vec3 fragColor;
		out vec4 outColor;

		void main()
		{
			outColor = vec4(fragColor, 1.0);
		}
	)GLSL";
public:
	GLuint program;
	GLuint vbo, ebo, vao;
	GLuint vsh, fsh;
	vector<Vertex> vertices;
	vector<int> indices;
	GLint uModel, uPersp;
	glm::mat4 model, perspective;

	bool isWireframe;

	State4_EBOC()
	{
		glClearColor(0.0, 0.0, 0.0, 1.0);
		program = glCreateProgram();
		vsh = compileShader(GL_VERTEX_SHADER, vertShader, __FILE__ ":VSH");
		fsh = compileShader(GL_FRAGMENT_SHADER, fragShader, __FILE__ ":FSH");
		glAttachShader(program, vsh);
		glAttachShader(program, fsh);
		linkProgram(program, __FILE__ ": " STR(__LINE__));
		glUseProgram(program);
		fprintf(stderr, "Shader OK\n");

		vertices.push_back({-0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f});
		vertices.push_back({ 0.5f, -0.5f, -0.5f, 0.0f, 1.0f, 0.0f});
		vertices.push_back({ 0.0f, -0.5f,  0.5f, 0.0f, 0.0f, 1.0f});
		vertices.push_back({ 0.5f,  0.5f,  0.0f, 1.0f, 0.0f, 1.0f});
		indices = {
			0, 1, 2,
			3, 1, 2,
			3, 2, 0,
			3, 0, 1
		};

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ebo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*vertices.size(), vertices.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int) * indices.size(), indices.data(), GL_STATIC_DRAW);

		fprintf(stderr, "VBO OK\n");

		GLint posAttrib = setAttrib(program, "position", 3, GL_FLOAT, GL_FALSE, offsetof(Vertex, x), sizeof(Vertex));
		GLint colAttrib = setAttrib(program, "vertColor", 3, GL_FLOAT, GL_FALSE, offsetof(Vertex, r), sizeof(Vertex));

		isWireframe = false;
		uModel = glGetUniformLocation(program, "model");
		uPersp = glGetUniformLocation(program, "perspective");
	}

	virtual ~State4_EBOC()
	{
		fprintf(stderr, "Cleaning Up\n");
		glDeleteProgram(program);
		glDeleteShader(vsh);
		glDeleteShader(fsh);
		glDeleteBuffers(1, &vbo);
		glDeleteVertexArrays(1, &vao);
		fprintf(stderr, "CleanUp Finished\n");
	}

	void draw(double dt)
	{
		static float taccum = 0.0;
		taccum += (float)dt;
		//model = glm::mat4();
		model = glm::translate(glm::mat4(), glm::vec3(0, 0, -1.5f));
		model = glm::rotate(model, glm::radians(taccum*360.0f), glm::vec3(0.5f, 0.5f, 0.5f));
		perspective = glm::perspectiveFov(glm::radians(75.0f), 800.f, 600.f, 0.1f, 1000.f);
		glEnable(GL_DEPTH_TEST);
		glUniformMatrix4fv(uModel, 1, GL_FALSE, glm::value_ptr(model));
		glUniformMatrix4fv(uPersp, 1, GL_FALSE, glm::value_ptr(perspective));
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
	}

	void update(double dt)
	{

	}

	void onButton(int button, int state)
	{
	}

	void onKey(int id, int state)
	{
	}

};

AState* makeState4()
{
	return new State4_EBOC();
}
