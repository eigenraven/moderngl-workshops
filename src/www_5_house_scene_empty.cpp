#include "wwwinc.h"
#include "www_utils.h"

class State5_SceneEmpty : public AState
{
	const GLchar* vertShader = R"GLSL(
		#version 330

		in vec2 position;
		in vec3 vertColor;

		out vec3 fragColor;

		uniform mat4 model, view, projection;

		void main()
		{
			fragColor = vertColor;
			gl_Position = vec4(position, 0.0, 1.0);
		}
	)GLSL";

	const GLchar* fragShader = R"GLSL(
		#version 330

		in vec3 fragColor;
		out vec4 outColor;

		void main()
		{
			outColor = vec4(fragColor, 1.0);
		}
	)GLSL";
public:
    glm::mat4 m_projection, m_camera;
    glm::vec3 cameraPosition;
    float cameraPitch, cameraYaw;
    const float CAMERA_ROTATE_YAW_SPEED = 50.0f;
    const float CAMERA_ROTATE_PITCH_SPEED = 40.0f;
    const float CAMERA_WALK_SPEED = 40.0f;

    void updateCamera()
    {
        m_camera = glm::mat4();
        m_camera = glm::translate(m_camera, -cameraPosition);
        m_camera = glm::rotate(m_camera, glm::degrees(cameraYaw), glm::vec3(0,1,0));
        m_camera = glm::rotate(m_camera, glm::degrees(cameraPitch), glm::vec3(1,0,0));
    }

    void loadResources()
    {
    }

    State5_SceneEmpty()
    {
        glClearColor(0.0, 0.0, 0.0, 1.0);
        m_projection = glm::perspectiveFov(glm::degrees(75.0f), 800.0f, 600.0f, 0.1f, 1000.0f);
        m_camera = glm::mat4();
        cameraPosition = glm::vec3(0,0,0);
        cameraPitch = 0.0f;
        cameraYaw = 0.0f;
        loadResources();
        blUp = blDown = blLeft = blRight = bmUp = bmDown = bmLeft = bmRight = false;
    }

    virtual ~State5_SceneEmpty()
    {
    	fprintf(stderr, "Cleaning Up\n");
    	fprintf(stderr, "CleanUp Finished\n");
    }

    void draw(double dt)
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    }

    bool blUp,blDown,blLeft,blRight;
    bool bmUp,bmDown,bmLeft,bmRight;

    void update(double dt)
    {
        if(blUp)
        {
            cameraPitch += CAMERA_ROTATE_PITCH_SPEED*dt;
            cameraPitch = glm::clamp(cameraPitch, -89.5f, 89.5f);
        }
        else if(blDown)
        {
            cameraPitch += -CAMERA_ROTATE_PITCH_SPEED*dt;
            cameraPitch = glm::clamp(cameraPitch, -89.5f, 89.5f);
        }
        if(blLeft)
        {
            cameraYaw += CAMERA_ROTATE_YAW_SPEED*dt;
            cameraYaw = std::fmod(cameraYaw, 360.0f);
        }
        else if(blRight)
        {
            cameraYaw += -CAMERA_ROTATE_YAW_SPEED*dt;
            cameraYaw = std::fmod(cameraYaw, 360.0f);
        }
        updateCamera();
        if(bmUp)
        {
            cameraPosition += glm::vec3(glm::vec4(0.0f, 0.0f, 1.0f, 0.0f) * m_camera * float(CAMERA_WALK_SPEED * dt));
        }
        else if(bmDown)
        {
            cameraPosition += glm::vec3(glm::vec4(0.0f, 0.0f, -1.0f, 0.0f) * m_camera * float(CAMERA_WALK_SPEED * dt));
        }
        if(bmRight)
        {
            cameraPosition += glm::vec3(glm::vec4(1.0f, 0.0f, 0.0f, 0.0f) * m_camera * float(CAMERA_WALK_SPEED * dt));
        }
        else if(bmLeft)
        {
            cameraPosition += glm::vec3(glm::vec4(-1.0f, 0.0f, 0.0f, 0.0f) * m_camera * float(CAMERA_WALK_SPEED * dt));
        }
    }

    void onButton(int button, int state)
    {
    }

    void onKey(int id, int state)
    {
        switch(id)
        {
        case GLFW_KEY_LEFT:
            blLeft = (state==GLFW_PRESS);
            break;
        case GLFW_KEY_RIGHT:
            blRight = (state==GLFW_PRESS);
            break;
        case GLFW_KEY_UP:
            blUp = (state==GLFW_PRESS);
            break;
        case GLFW_KEY_DOWN:
            blDown = (state==GLFW_PRESS);
            break;
        case GLFW_KEY_A:
            bmLeft = (state==GLFW_PRESS);
            break;
        case GLFW_KEY_D:
            bmRight = (state==GLFW_PRESS);
            break;
        case GLFW_KEY_W:
            bmUp = (state==GLFW_PRESS);
            break;
        case GLFW_KEY_S:
            bmDown = (state==GLFW_PRESS);
            break;
        case GLFW_KEY_SLASH:
            if(state==GLFW_PRESS)
            {
                fprintf(stderr, "Camera:\n Position: (%6.2f %6.2f %6.2f)\n Pitch: %6.2f\n Yaw: %6.2f\n",
                    (float)cameraPosition.x, (float)cameraPosition.y, (float)cameraPosition.z,
                    (float)cameraPitch, (float)cameraYaw);
            }
            break;
        }
    }

};

AState* makeState5()
{
    return new State5_SceneEmpty();
}
