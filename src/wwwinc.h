#pragma once

#define GLM_FORCE_RADIANS

#include <cstdlib>
#include <cstdio>
#include <stddef.h>
#include <cmath>
#include <cstring>
#include <algorithm>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <utility>
#include <unordered_map>
#include "glad.h"
#include <GLFW/glfw3.h>
#include "glm/fwd.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "stb_image.h"
#include "tinyobj.h"

using namespace std;

class AState
{
public:
	virtual ~AState();
    virtual void draw(double dt) = 0;
    virtual void update(double dt) = 0;
    virtual void onButton(int button, int state) = 0;
    virtual void onKey(int id, int state) = 0;
    virtual void onMouseMove(double x, double y){}
};

extern GLFWwindow* window;
extern AState* state;

void createWindow();

AState* makeState1();
AState* makeState2();
AState* makeState3();
AState* makeState4();
AState* makeState5();
AState* makeState6();
