#include "wwwinc.h"
#include "www_utils.h"

struct HSVertex
{
	/// Position
	float x, y, z;
	/// Normal
	float nx, ny, nz;
	/// Diffuse Color
	float dr, dg, db;
	/// Emission Color
	float er, eg, eb;
	/// Specular Color
	float sr, sg, sb;
	/// Texture coords
	float u, v;
	/// Transparency
	float alpha;
};

struct SkyVertex
{
	/// Position
	float x, y, z;
};

const SkyVertex skyCubeData[14] = {
	{ -1.0f, -1.0f, -1.0f },
	{ -1.0f,  1.0f, -1.0f },
	{  1.0f, -1.0f, -1.0f },
	{  1.0f,  1.0f, -1.0f },
	{  1.0f,  1.0f,  1.0f },
	{ -1.0f,  1.0f, -1.0f },
	{ -1.0f,  1.0f,  1.0f },
	{ -1.0f, -1.0f,  1.0f },
	{  1.0f,  1.0f,  1.0f },
	{  1.0f, -1.0f,  1.0f },
	{  1.0f, -1.0f, -1.0f },
	{ -1.0f, -1.0f,  1.0f },
	{ -1.0f, -1.0f, -1.0f },
	{ -1.0f,  1.0f, -1.0f }
};

struct DrawObject
{
	/// Index into EBO
	int start;
	int numIndices;
	int texture;
};

enum LightMode
{
	LIGHT_STATIC,
	LIGHT_CAMERA,
	LIGHT_ORBIT,
	LIGHT_MODEMAX
};

class State6_Scene : public AState
{
	const GLchar* vertShader = R"GLSL(
		#version 330

		in vec3 position;
		in vec3 normal;
		in vec3 diffuse;
		in vec3 emission;
		in vec3 specular;
		in vec2 texcoords;
		in float alpha;

		out vec3 fPosition;
		out vec3 fNormal;
		out vec3 fDiffuse;
		out vec3 fEmission;
		out vec3 fSpecular;
		out vec2 fTexcoords;
		out float fAlpha;

		uniform mat4 view, projection;

		void main()
		{
			gl_Position = projection * view * vec4(position, 1.0);
			fNormal = normal;
			fDiffuse = diffuse;
			fEmission = emission;
			fSpecular = specular;
			fTexcoords = texcoords;
			fPosition = position;
			fAlpha = alpha;
		}
	)GLSL";

	const GLchar* fragShader = R"GLSL(
		#version 330

		uniform vec3 eyePosition;
		uniform vec3 ambientLight;
		uniform vec3 pointLightPosition;
		uniform vec3 pointLightColor;

		uniform int hasTexture;
		uniform sampler2D materialTexture;

		in vec3 fPosition;
		in vec3 fNormal;
		in vec3 fDiffuse;
		in vec3 fEmission;
		in vec3 fSpecular;
		in vec2 fTexcoords;
		in float fAlpha;
		out vec4 outColor;

		void main()
		{
			/// texture
			vec3 texColor;
			if(hasTexture > 0)
			{
				texColor = texture(materialTexture, fTexcoords).xyz;
			}
			else
			{
				texColor = vec3(1.0);
			}
			vec3 surfColor = fDiffuse * texColor;
			///ambient-only
			//outColor = vec4(ambientLight * surfColor, 1.0);

			/// general parameters
			float lightDistance = length(pointLightPosition-fPosition);

			/// diffuse parameters
			// surface to light
			vec3 invLightDir = normalize(pointLightPosition - fPosition);
			// diffuse brightness factor
			float diffuseFactor = dot(fNormal, invLightDir);
			diffuseFactor = clamp(diffuseFactor, 0, 1);

			/// specular parameters
			// specular reflection angle
			vec3 reflectDir = reflect(-invLightDir, fNormal);
			// surface to camera direction
			vec3 surfaceToCamera = normalize(eyePosition - fNormal);

			float specularFactor = dot(surfaceToCamera, reflectDir);
			specularFactor = clamp(specularFactor, 0, 1);
			if(diffuseFactor==0.0)
				specularFactor = 0.0;

			/// attenuation
			float attenuation = 1.0 / ( 1.0 + pow(lightDistance, 2) );

			/// output
			vec3 color = ambientLight * surfColor
					+ surfColor * pointLightColor * diffuseFactor * attenuation
					+ fSpecular * pointLightColor * pow(specularFactor, 5) * attenuation;
			color = clamp(color, 0.0, 1.0);
			outColor = vec4(color, fAlpha);
		}
	)GLSL";

	const GLchar* skyVertShader = R"GLSL(
		#version 330

		in vec3 position;

		out vec3 fPosition;

		uniform mat4 view, projection;

		void main()
		{
			gl_Position = projection * view * vec4(position, 1.0);
			fPosition = position;
		}
	)GLSL";

	const GLchar* skyFragShader = R"GLSL(
		#version 330

		uniform samplerCube envMap;

		in vec3 fPosition;
		out vec4 outColor;

		void main()
		{
			vec3 envDir = normalize(fPosition);
			outColor = vec4(texture(envMap, envDir).xyz, 1.0);
		}
	)GLSL";

public:
	glm::mat4 m_projection, m_camera, m_camera_notranslate;
	glm::vec3 cameraPosition, lightPosition, lightColor;
	float cameraPitch, cameraYaw;
	const float CAMERA_ROTATE_YAW_SPEED = 90.0f;
	const float CAMERA_ROTATE_PITCH_SPEED = 40.0f;
	const float CAMERA_WALK_SPEED = 60.0f;
	vector<DrawObject> objects;
	vector<tinyobj::shape_t> scShapes;
	vector<tinyobj::material_t> scMaterials;
	vector<GLint> materialTextures;
	LightMode lightMode;

	GLuint program;
	GLuint vaoId;
	GLuint vboId;
	GLuint eboId;

	GLuint skyProgram;
	GLuint skyVao;
	GLuint skyVbo;
	GLuint envMap;
	GLint uView, uProj, uAmbient, uPointPosition, uPointColor, uEye, uHasTexture, uTexture;
	GLint usView, usProj, usEnvMap;

	void updateCamera()
	{
		m_camera = glm::mat4();
		m_camera = glm::rotate(m_camera, cameraPitch*glm::pi<float>()/180.0f, glm::vec3(1,0,0));
		m_camera = glm::rotate(m_camera, cameraYaw*glm::pi<float>()/180.0f, glm::vec3(0,1,0));
		m_camera_notranslate = m_camera;
		m_camera = glm::translate(m_camera, -cameraPosition);
	}

	void loadResources()
	{
		// shader
		program = glCreateProgram();
		auto vsh = compileShader(GL_VERTEX_SHADER, vertShader, __FILE__ ":VSH");
		auto fsh = compileShader(GL_FRAGMENT_SHADER, fragShader, __FILE__ ":FSH");
		glAttachShader(program, vsh);
		glAttachShader(program, fsh);
		linkProgram(program, __FILE__ ": " STR(__LINE__));
		glUseProgram(program);

		skyProgram = glCreateProgram();
		auto svsh = compileShader(GL_VERTEX_SHADER, skyVertShader, __FILE__ ":SVSH");
		auto sfsh = compileShader(GL_FRAGMENT_SHADER, skyFragShader, __FILE__ ":SFSH");
		glAttachShader(skyProgram, svsh);
		glAttachShader(skyProgram, sfsh);
		linkProgram(skyProgram, __FILE__ ": " STR(__LINE__));
		glUseProgram(skyProgram);
		glUseProgram(program);

		printf("Shaders OK\n");
		// scene
		string err;
		if(!tinyobj::LoadObj(scShapes, scMaterials, err, "res/scena.obj", "res/"))
		{
			fprintf(stderr, "Error loading OBJ scene: %s\n", err.c_str());
			exit(1);
		}
		printf("Loaded OBJ scene:");
		vector<HSVertex> verts;
		vector<int> elements;
		typedef pair<int, int> vertKey; // (vertId, materialId)
		unordered_map<vertKey, int> vertIds;
		int totalVerts = 0;
		for(size_t ishape=0;ishape<scShapes.size();ishape++)
		{
			vertIds.clear();
			tinyobj::shape_t& shape = scShapes[ishape];
			tinyobj::mesh_t& mesh = shape.mesh;
			size_t numVerts = mesh.positions.size()/3;
			DrawObject dobj;
			dobj.numIndices = (int)mesh.indices.size();
			dobj.start = (int)elements.size();
			if(scMaterials[mesh.material_ids[0]].diffuse_texname.length() > 0)
			{
				dobj.texture = mesh.material_ids[0];
			}
			else
			{
				dobj.texture = -1;
			}
			for(size_t ii=0; ii<dobj.numIndices; ii++)
			{
				int face = ii/3;
				int matid = mesh.material_ids[face];
				int i = mesh.indices[ii];
				vertKey key = make_pair(i, matid);
				if(vertIds.count(key) == 0)
				{
					vertIds[key] = totalVerts;
					tinyobj::material_t& mat = scMaterials[matid];
					HSVertex v;
					v.x = mesh.positions[3*i];
					v.y = -mesh.positions[3*i + 1];
					v.z = mesh.positions[3*i + 2];
					if(mesh.normals.size()>0)
					{
						v.nx = mesh.normals[3*i];
						v.ny = -mesh.normals[3*i + 1];
						v.nz = mesh.normals[3*i + 2];
					}
					if(mesh.texcoords.size()>0)
					{
						v.u = mesh.texcoords[2*i];
						v.v = mesh.texcoords[2*i + 1];
					}
					if(matid < scMaterials.size())
					{
						v.alpha = mat.dissolve;
						v.dr = mat.diffuse[0];
						v.dg = mat.diffuse[1];
						v.db = mat.diffuse[2];
						v.er = mat.emission[0];
						v.eg = mat.emission[1];
						v.eb = mat.emission[2];
						v.sr = mat.specular[0];
						v.sg = mat.specular[1];
						v.sb = mat.specular[2];
					}
					else
					{
						v.alpha = 1.0f;
						v.dr = 1.0f;
						v.dg = 0.0f;
						v.db = 1.0f;
						v.er = 0.5f;
						v.eg = 0.0f;
						v.eb = 0.5f;
						v.sr = 0.0f;
						v.sg = 0.5f;
						v.sb = 0.0f;
					}
					verts.push_back(v);
					elements.push_back(totalVerts);
					totalVerts ++;
				}
				else
				{
					elements.push_back(vertIds[key]);
				}
			}
			objects.push_back(dobj);
		}
		materialTextures.resize(scMaterials.size(), 0);
		for(int i=0; i<scMaterials.size();i++)
		{
			tinyobj::material_t& mat = scMaterials[i];
			if(mat.diffuse_texname.length()>0)
			{
				int w, h, comp;
				mat.diffuse_texname = string("res/") + mat.diffuse_texname;
				unsigned char* data = stbi_load(mat.diffuse_texname.c_str(), &w, &h, &comp, 3);
				GLuint texid;
				glGenTextures(1, &texid);
				glBindTexture(GL_TEXTURE_2D, texid);
				materialTextures[i] = texid;
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
				glGenerateMipmap(GL_TEXTURE_2D);
				free(data);
			}
		}
		printf("Loaded %d vertices, %d indices, %d materials.\n", int(verts.size()), int(elements.size()), int(scMaterials.size()));
		fflush(stdout);
		// additional textures
		envMap = loadCubemap("res/skybox1024/");
		glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
		// GL buffers
		glGenVertexArrays(1, &vaoId);
		glBindVertexArray(vaoId);
		glGenBuffers(1, &vboId);
		glGenBuffers(1, &eboId);
		glBindBuffer(GL_ARRAY_BUFFER, vboId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId);
		glBufferData(GL_ARRAY_BUFFER, verts.size()*sizeof(HSVertex), verts.data(), GL_STATIC_DRAW);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements.size()*sizeof(int), elements.data(), GL_STATIC_DRAW);
		setAttrib(program, "position", 3, GL_FLOAT, GL_FALSE, offsetof(HSVertex, x), sizeof(HSVertex));
		setAttrib(program, "normal", 3, GL_FLOAT, GL_FALSE, offsetof(HSVertex, nx), sizeof(HSVertex));
		setAttrib(program, "diffuse", 3, GL_FLOAT, GL_FALSE, offsetof(HSVertex, dr), sizeof(HSVertex));
		setAttrib(program, "specular", 3, GL_FLOAT, GL_FALSE, offsetof(HSVertex, sr), sizeof(HSVertex));
		setAttrib(program, "emission", 3, GL_FLOAT, GL_FALSE, offsetof(HSVertex, er), sizeof(HSVertex));
		setAttrib(program, "texcoords", 2, GL_FLOAT, GL_FALSE, offsetof(HSVertex, u), sizeof(HSVertex));
		setAttrib(program, "alpha", 1, GL_FLOAT, GL_FALSE, offsetof(HSVertex, alpha), sizeof(HSVertex));
		uView = glGetUniformLocation(program, "view");
		uProj = glGetUniformLocation(program, "projection");
		uAmbient = glGetUniformLocation(program, "ambientLight");
		uEye = glGetUniformLocation(program, "eyePosition");
		uPointColor = glGetUniformLocation(program, "pointLightColor");
		uPointPosition = glGetUniformLocation(program, "pointLightPosition");
		uHasTexture = glGetUniformLocation(program, "hasTexture");
		uTexture = glGetUniformLocation(program, "materialTexture");

		glGenVertexArrays(1, &skyVao);
		glUseProgram(skyProgram);
		glBindVertexArray(skyVao);
		glGenBuffers(1, &skyVbo);
		glBindBuffer(GL_ARRAY_BUFFER, skyVbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(skyCubeData), skyCubeData, GL_STATIC_DRAW);
		setAttrib(skyProgram, "position", 3, GL_FLOAT, GL_FALSE, offsetof(SkyVertex, x), sizeof(SkyVertex));
		usView = glGetUniformLocation(skyProgram, "view");
		usProj = glGetUniformLocation(skyProgram, "projection");
		usEnvMap = glGetUniformLocation(skyProgram, "envMap");

		glUseProgram(program);
	}

	State6_Scene()
	{
		glClearColor(0.0, 0.0, 0.0, 1.0);
		m_projection = glm::perspectiveFov(glm::degrees(75.0f), 800.0f, 600.0f, 0.1f, 1000.0f);
		m_camera = glm::mat4();
		cameraPosition = glm::vec3(0,0,0);
		cameraPitch = 0.0f;
		cameraYaw = 0.0f;
		loadResources();
		blUp = blDown = blLeft = blRight = bmUp = bmDown = bmLeft = bmRight = false;
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		lightColor = glm::vec3(0.8f, 0.8f, 0.8f) * 500.0f; // color*power
		lightMode = LIGHT_ORBIT;
	}

	virtual void onMouseMove(double x, double y)
	{
		static double lastX = 0.125, lastY = 0.125;
		if(lastX == 0.125)
		{
			lastX = x;
			lastY = y;
		}
		cameraPitch += (lastY-y)*0.1;
		cameraPitch = glm::clamp(cameraPitch, -89.5f, 89.5f);
		cameraYaw += (lastX-x)*0.15;
		cameraYaw = std::fmod(cameraYaw, 360.0f);
		updateCamera();
		lastY = y;
		lastX = x;
	}

	virtual ~State6_Scene()
	{
		fprintf(stderr, "Cleaning Up\n");
		glDeleteBuffers(1, &vboId);
		fprintf(stderr, "CleanUp Finished\n");
	}

	void draw(double dt)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		// draw skybox
		glDepthMask(GL_FALSE); // disable depth writes
		glUseProgram(skyProgram);
		glBindVertexArray(skyVao);
		glBindBuffer(GL_ARRAY_BUFFER, skyVbo);
		glBindTexture(GL_TEXTURE_CUBE_MAP, envMap);
		glUniformMatrix4fv(usProj, 1, GL_FALSE, glm::value_ptr(m_projection));
		glUniformMatrix4fv(usView, 1, GL_FALSE, glm::value_ptr(m_camera_notranslate));
		glUniform1i(usEnvMap, 0);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 14);
		// draw scene
		glUseProgram(program);
		glBindVertexArray(vaoId);
		glBindBuffer(GL_ARRAY_BUFFER, vboId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, eboId);
		glDepthMask(GL_TRUE);
		glUniformMatrix4fv(uProj, 1, GL_FALSE, glm::value_ptr(m_projection));
		glUniformMatrix4fv(uView, 1, GL_FALSE, glm::value_ptr(m_camera));
		glUniform3f(uAmbient, 0.4f, 0.5f, 0.6f);
		glUniform3fv(uEye, 1, glm::value_ptr(cameraPosition));
		glUniform3fv(uPointPosition, 1, glm::value_ptr(lightPosition));
		glUniform3fv(uPointColor, 1, glm::value_ptr(lightColor));
		glUniform1i(uTexture, 0);
		for(DrawObject& obj : objects)
		{
			if(obj.texture>=0)
			{
				glUniform1i(uHasTexture, 1);
				glBindTexture(GL_TEXTURE_2D, materialTextures[obj.texture]);
			}
			else
			{
				glUniform1i(uHasTexture, 0);
			}
			int* nl = nullptr;
			glDrawElements(GL_TRIANGLES, obj.numIndices, GL_UNSIGNED_INT, nl + obj.start);
		}
	}

	bool blUp,blDown,blLeft,blRight;
	bool bmUp,bmDown,bmLeft,bmRight;

	double accumDt = 0.0;

	void update(double dt)
	{
		accumDt += dt;
		accumDt = fmod(accumDt, 360.0*3.14159);
		if(blUp)
		{
			cameraPitch += CAMERA_ROTATE_PITCH_SPEED*dt;
			cameraPitch = glm::clamp(cameraPitch, -89.5f, 89.5f);
		}
		else if(blDown)
		{
			cameraPitch += -CAMERA_ROTATE_PITCH_SPEED*dt;
			cameraPitch = glm::clamp(cameraPitch, -89.5f, 89.5f);
		}
		if(blLeft)
		{
			cameraYaw += CAMERA_ROTATE_YAW_SPEED*dt;
			cameraYaw = std::fmod(cameraYaw, 360.0f);
		}
		else if(blRight)
		{
			cameraYaw += -CAMERA_ROTATE_YAW_SPEED*dt;
			cameraYaw = std::fmod(cameraYaw, 360.0f);
		}
		updateCamera();
		if(bmUp)
		{
			cameraPosition += glm::vec3(glm::vec4(0.0f, 0.0f, -1.0f, 0.0f) * m_camera * float(CAMERA_WALK_SPEED * dt));
		}
		else if(bmDown)
		{
			cameraPosition += glm::vec3(glm::vec4(0.0f, 0.0f, 1.0f, 0.0f) * m_camera * float(CAMERA_WALK_SPEED * dt));
		}
		if(bmRight)
		{
			cameraPosition += glm::vec3(glm::vec4(-1.0f, 0.0f, 0.0f, 0.0f) * m_camera * float(CAMERA_WALK_SPEED * dt));
		}
		else if(bmLeft)
		{
			cameraPosition += glm::vec3(glm::vec4(1.0f, 0.0f, 0.0f, 0.0f) * m_camera * float(CAMERA_WALK_SPEED * dt));
		}
		if(lightMode == LIGHT_CAMERA)
		{
			lightPosition = cameraPosition;
		}
		else if(lightMode == LIGHT_ORBIT)
		{
			lightPosition = glm::vec3(0,-10,0) + glm::vec3(20,0,0) * float(sin(accumDt)) + glm::vec3(0,0,20) * float(cos(accumDt));
		}
	}

	void onButton(int button, int state)
	{
		if(state == GLFW_PRESS)
		{
			if(button == GLFW_MOUSE_BUTTON_LEFT)
			{
				lightPosition = cameraPosition;
				lightMode = LIGHT_STATIC;
			}
			else if(button == GLFW_MOUSE_BUTTON_RIGHT)
			{
				lightMode = LightMode(int(lightMode) + 1);
				if(lightMode == LIGHT_MODEMAX)
					lightMode = LIGHT_STATIC;
				switch(lightMode)
				{
				case LIGHT_STATIC:
					fprintf(stderr, "Light: Static\n");
					break;
				case LIGHT_CAMERA:
					fprintf(stderr, "Light: Camera flashlight\n");
					break;
				case LIGHT_ORBIT:
					fprintf(stderr, "Light: Orbit\n");
					break;
				default:
					break;
				}
			}
		}
	}

	void onKey(int id, int state)
	{
		switch(id)
		{
		case GLFW_KEY_LEFT:
			blLeft = (state!=GLFW_RELEASE);
			break;
		case GLFW_KEY_RIGHT:
			blRight = (state!=GLFW_RELEASE);
			break;
		case GLFW_KEY_UP:
			blUp = (state!=GLFW_RELEASE);
			break;
		case GLFW_KEY_DOWN:
			blDown = (state!=GLFW_RELEASE);
			break;
		case GLFW_KEY_A:
			bmLeft = (state!=GLFW_RELEASE);
			break;
		case GLFW_KEY_D:
			bmRight = (state!=GLFW_RELEASE);
			break;
		case GLFW_KEY_W:
			bmUp = (state!=GLFW_RELEASE);
			break;
		case GLFW_KEY_S:
			bmDown = (state!=GLFW_RELEASE);
			break;
		case GLFW_KEY_SLASH:
			if(state!=GLFW_RELEASE)
			{
				fprintf(stderr, "Camera:\n Position: (%6.2f %6.2f %6.2f)\n Pitch: %6.2f\n Yaw: %6.2f\n",
					(float)cameraPosition.x, (float)cameraPosition.y, (float)cameraPosition.z,
					(float)cameraPitch, (float)cameraYaw);
			}
			break;
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, 1);
			break;
		}
	}

};

AState* makeState6()
{
	return new State6_Scene();
}
