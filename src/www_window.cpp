#include "wwwinc.h"

GLFWwindow* window;

void glfwErrorCallback(int code, const char* msg)
{
	fprintf(stderr, "Error %d: %s\n", code, msg);
}

void setFpsMouseMode(GLFWwindow* window, bool relativeMouse)
{
	glfwSetInputMode(window, GLFW_CURSOR, relativeMouse ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
}

void glfwKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, 1);
	state->onKey(key, action);
}

void glfwMouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
	state->onButton(button, action);
}

void glfwMouseMoveCallback(GLFWwindow* window, double x, double y)
{
	state->onMouseMove(x, y);
}

void glfwWindowFocusCallback(GLFWwindow* window, int focused)
{
	static bool relmouse = GLFW_CURSOR_NORMAL;
	if(focused == 0)
	{
		relmouse = glfwGetInputMode(window, GLFW_CURSOR);
	}
	else
	{
		glfwSetInputMode(window, GLFW_CURSOR, relmouse);
	}
}


void createWindow()
{
	glfwSetErrorCallback(&glfwErrorCallback);
	if(!glfwInit())
	{
		throw new runtime_error("Could not init GLFW");
	}
	glfwWindowHint(GLFW_DOUBLEBUFFER, 1);
	glfwWindowHint(GLFW_RESIZABLE, 0);
	glfwWindowHint(GLFW_DEPTH_BITS, 8);
	glfwWindowHint(GLFW_STENCIL_BITS, 8);
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	window = glfwCreateWindow(1024, 768, "Warsztaty WWW - Nowoczesny OpenGL", nullptr, nullptr);
	if(window == nullptr)
	{
		throw new runtime_error("Could not create window");
	}
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, glfwKeyCallback);
	glfwSetMouseButtonCallback(window, glfwMouseButtonCallback);
	glfwSetCursorPosCallback(window, glfwMouseMoveCallback);
	glfwSetWindowFocusCallback(window, glfwWindowFocusCallback);
	if(!gladLoadGLLoader((GLADloadproc)&glfwGetProcAddress))
	{
		throw new runtime_error("Could not load OpenGL procedures");
	}
	glfwSwapInterval(1);
	fprintf(stderr, "Loaded GL %d.%d\n", GLVersion.major, GLVersion.minor);
}
