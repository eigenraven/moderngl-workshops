#include "wwwinc.h"
#include "www_utils.h"

class State1_HT : public AState
{
	const GLchar* vertShader = R"GLSL(
		#version 330

		in vec2 position;

		void main()
		{
			gl_Position = vec4(position, 0.0, 1.0);
		}
	)GLSL";

	const GLchar* fragShader = R"GLSL(
		#version 330

		out vec4 outColor;

		void main()
		{
			outColor = vec4(1.0, 1.0, 1.0, 1.0);
		}
	)GLSL";
public:
	GLuint program;
	GLuint vbo, vao;
	GLuint vsh, fsh;

	State1_HT()
	{
		glClearColor(0.0, 0.0, 0.0, 1.0);
		program = glCreateProgram();
		vsh = compileShader(GL_VERTEX_SHADER, vertShader, __FILE__ ":VSH");
		fsh = compileShader(GL_FRAGMENT_SHADER, fragShader, __FILE__ ":FSH");
		glAttachShader(program, vsh);
		glAttachShader(program, fsh);
		linkProgram(program, __FILE__ ": " STR(__LINE__));
		glUseProgram(program);
		fprintf(stderr, "Shader OK\n");

		float verts[] = {
			0.0f, 0.5f,
			0.5f, -0.5f,
			-0.5f, -0.5f
		};

		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
		fprintf(stderr, "VBO OK\n");

		GLint posAttrib = glGetAttribLocation(program, "position");
		glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(posAttrib);
	}

	virtual ~State1_HT()
	{
		fprintf(stderr, "Cleaning Up\n");
		glDeleteProgram(program);
		glDeleteShader(vsh);
		glDeleteShader(fsh);
		glDeleteBuffers(1, &vbo);
		glDeleteVertexArrays(1, &vao);
		fprintf(stderr, "CleanUp Finished\n");
	}

	void draw(double dt)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glDrawArrays(GL_TRIANGLES, 0, 3);
	}

	void update(double dt)
	{

	}

	void onButton(int button, int state)
	{

	}

	void onKey(int id, int state)
	{

	}

};

AState* makeState1()
{
	return new State1_HT();
}
